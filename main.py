from tkinter import *
from tkinter import messagebox
from random import *
import pyperclip as pp
import json




def generate_password():

    l = ['q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'z', 'x', 'c',
         'v', 'b', 'n', 'm', 'Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P', 'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K',
         'L', 'Z', 'X', 'C', 'V', 'B', 'N', 'M']
    s = ['@', '#', '$', '%', '^', '&', '*', '(', ')', '?']
    n = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0']
    letter=5
    symbol=2
    number=2
    password_list = []

    for i in range(0, letter):
        password_list.append(choice(l))

    for i in range(0, symbol):
        password_list.append(choice(s))

    for i in range(0, number):
        password_list.append(choice(n))

    password = ""
    # print(password_list)
    shuffle(password_list)

    for n in range(0, len(password_list)):
        password += str(password_list[n])

    password_entry.delete(0,END)
    pp.copy(password)
    password_entry.insert(0,password)


def Search_method():
    website_name=website_entry.get()
    try:
        with open("data.json","r") as file:
            data=json.load(file)
    except:
        messagebox.askokcancel(title="add details",message="Please firstly add the details ")
    else:
        if website_name in data:
            messagebox.askokcancel(title=website_name,message=f"Your details : \nemail : {data[website_name]['email']}\npassword :{data[website_name]['password']} ")
        else:
            messagebox.askokcancel(title="Oops",message="Doest present in the database try adding new ...")

def save():
    website=website_entry.get()
    email=email_entry.get()
    password=password_entry.get()
    new_data={website:{"email":email,"password":password}}

    if len(website)==0 or len(password)==0:
        messagebox.askokcancel(title="oops",message="some fields are not added plz add this first")
        return
    is_ok = messagebox.askokcancel(title=website,message=f"The details are \nEmail : {email}\nPassword : {password}\nwant to Add this sucessfully \n")

    if is_ok:
        try:
            with  open("data.json","r") as file:
                # file.write(f"{website}  ||  {email}  ||  {password}  \n")
                #reading the old data
                data=json.load(file)
                #updating old data
                data.update(new_data)
        except:
            with  open("data.json","w") as file:
                json.dump(new_data,file,indent=4)

        else:
            with  open("data.json","w") as file:
                #inserting updated data
                json.dump(data,file,indent=4)

        finally:
            website_entry.delete(0, END)
            # email_entry.delete(0,END)
            password_entry.delete(0, END)







window=Tk()
window.title("Password generator")
window.config(pady=50,padx=50)

canvas=Canvas(width=400,height=300)
photo_img=PhotoImage(file="pad-lock-with-cloud-2254ld.png")
canvas.create_image(200,150,image=photo_img)
canvas.grid(padx=20,pady=20,columnspan=3)


#labels
website_label=Label(text="Website :",font=(9))
website_label.grid(row=1,column=0)
email_label=Label(text="Email/username :",font=(9))
email_label.grid(row=2,column=0)
password_label=Label(text="password :",font=(9))
password_label.grid(row=3,column=0)

#entrys
website_entry=Entry(width=21)
website_entry.focus()
website_entry.grid(row=1,column=1)
email_entry=Entry(width=40)
email_entry.insert(0,"demo@gmail.com")
email_entry.grid(row=2,column=1,columnspan=2)

password_entry=Entry(width=21)
password_entry.grid(row=3,column=1)

#buttons

searc_button=Button(text='search',width=14,command=Search_method)
searc_button.grid(row=1,column=2,columnspan=2)

add_button=Button(text='Add',width=36,command=save)
add_button.grid(row=4,column=1,columnspan=2)

generate_password_button=Button(text="Generate_password",command=generate_password)
generate_password_button.grid(row=3,column=2,columnspan=2)

window.mainloop()